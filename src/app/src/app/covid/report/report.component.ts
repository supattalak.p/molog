import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})

export class ReportComponent implements OnInit {
  data:any = []
  url = "https://static.easysunday.com/covid-19/getTodayCases.json";
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.http.get(this.url).subscribe((res)=>{
      this.data = res
      console.log(this.data)
    })
  }
}
