import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

export interface Config {
  country: string;
  textfile: string;
  date: any;
}

@Injectable({
  providedIn: 'root',
})
export class ApicallService {
  url = 'https://static.easysunday.com/covid-19/getTodayCases.json';
  constructor(private http: HttpClient) {}


  getData() {
    return this.http.get<Config>(this.url);
  }
}
